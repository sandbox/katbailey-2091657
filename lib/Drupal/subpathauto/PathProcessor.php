<?php

/**
 * @file
 * Contains Drupal\subpathauto\PathProcessor.
 */

namespace Drupal\subpathauto;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Processes the inbound path using path alias lookups.
 */
class PathProcessor implements InboundPathProcessorInterface {

  protected $pathProcessor;

  public function __construct(InboundPathProcessorInterface $path_processor) {
    $this->pathProcessor = $path_processor;
  }

  /**
   * Implements Drupal\Core\PathProcessor\InboundPathProcessorInterface::processInbound().
   */
  public function processInbound($path, Request $request) {
    $request_path = $request->getPathInfo();
    if (ltrim($request_path, '/') !== $path) {
      return $path;
    }
    $original_path = $path;
    $subpath = array();
    while($path_array = explode('/', $path)) {
      $subpath[] = array_pop($path_array);
      if (empty($path_array)) {
        break;
      }
      $path = implode('/', $path_array);
      $processed_path = $this->pathProcessor->processInbound($path, $request);
      if ($processed_path !== $path) {
        $path = $processed_path . '/' . implode('/', array_reverse($subpath));
        return $path;
      }
    }
    return $original_path;
  }

}
